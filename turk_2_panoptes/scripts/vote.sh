#!/bin/bash

rm analyzed_results/agreement.tsv
touch analyzed_results/agreement.tsv

rm analyed_results/performance.tsv
cp analyzed_results/performance.template analyzed_results/performance.tsv

trap "exit" INT

for subj_set in `ls -d user_results/annotations/*/`
do

    python ./scripts/expert_vote.py `find $subj_set -type f`
    python ./scripts/draw_votes.py `find $subj_set -type f`
	
done



