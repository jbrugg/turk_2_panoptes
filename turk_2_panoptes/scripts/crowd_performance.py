#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

def gold_eval(datapts,goldpts): 
    

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0,0)
    elif gold_n == 0:
        return (0,exp_n,0)
    elif exp_n == 0:
        return (0,0,gold_n)
        
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts) 
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    return (tp,fp,fn)

        
with open("/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/data/zoo_conv.json",'r') as in_file:
    conv_data = json.load(in_file)

with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/turk2zoo_conv.json",'r') as in_file:
    zoo2turk = json.load(in_file)
    
perf_data = []

for subj_id in os.listdir("/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/user_results/annotations/"):
    try:
        gold_id = conv_data[subj_id]
    except KeyError:
        print 'KEYERROR'
        continue

    gold_data = []
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        
        for line in reader:
            gold_data.append(map(lambda x: float(x), line))


    for user_file in os.listdir("/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/user_results/annotations/%s/" % subj_id):
        
        with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/user_results/annotations/%s/%s' % (subj_id,user_file),'r') as in_file:
            user_id = user_file

            reader = csv.reader(in_file,delimiter='\t')
            user_data = []
            
            for line in reader:
                user_data.append(map(lambda x: float(x), line))

        performance_vec = gold_eval(user_data,gold_data)

        subjZooID = zoo2turk[subj_id]
        
        entry = [subjZooID,user_id]
        entry.extend(performance_vec)
        entry.append(len(user_data))
        perf_data.append(entry)
        

with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/analysis/crowd_performance.tsv','w') as out_file:
    writer = csv.writer(out_file,delimiter='\t')
    writer.writerow(['subj_id','user_id','tp','fp','fn','anno_length'])

    for entry in perf_data:
        writer.writerow(entry)

