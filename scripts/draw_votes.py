#!/usr/bin/python

import argparse
import json
import os.path
import csv
import argparse

from PIL import Image, ImageDraw

def main():
    
    parser = argparse.ArgumentParser(description="Draw votes n stuff")
    parser.add_argument('results_files',metavar='vote_n',type=str,nargs='+',help='result files')

    args = parser.parse_args()
    
    results = args.results_files
    
    results_vec = []
    voted_data = []
    for vote_file in results:
        if 'vote.tsv' in vote_file:
            with open(vote_file,'r') as in_file:
                tsv_reader = csv.reader(in_file,delimiter='\t')
                for line in tsv_reader:
                    voted_data.append(map(lambda x: float(x),line))

        else:
            with open(vote_file,'r') as in_file:
                tsv_reader = csv.reader(in_file,delimiter='\t')
                entry = []
                for line in tsv_reader:
                    entry.append(map(lambda x: float(x),line))
            results_vec.append(entry)

    with open('data/code_img_file.json','r') as in_file:
        file_data = json.load(in_file)
        
    with open('data/glander_conv.json','r') as in_file:
        glander_conv = json.load(in_file)

    with open('data/glander.json','r') as in_file:
        glander_data = json.load(in_file)

    gold_data = {}

    for key,value in glander_data.iteritems():
        try:
            gold_data[glander_conv[key]] = value
        except:
            pass

    #first figure out what image ur on
    try:
        id = os.path.dirname(results[0]).split('/')[-1]
        
    except:
        return
    
    in_file = 'data/img_files/'+file_data[id]
    
    vote_out_file = 'analyzed_results/vote_only/'+id+'.jpg'
    
    draw_circles(results_vec,in_file,vote_out_file)
    
    if id in gold_data.keys():
        g_dat = gold_data[id]
        gold_out_file =  'analyzed_results/with_gold/'+id+'.jpg'

        valid_pts = [filter(lambda x: x[2] > 1, voted_data)]
        
        draw_circles(valid_pts,in_file,gold_out_file)
        draw_gold(g_dat,gold_out_file)
        
        color_file =  'analyzed_results/coloured/'+id+'.jpg'
        draw_votes(voted_data,in_file,color_file)
        draw_gold(g_dat,color_file)
    else:
        color_file =  'analyzed_results/coloured/'+id+'.jpg'
        draw_votes(voted_data,in_file,color_file)

    return

def draw_gold(points,filename):
    with Image.open(filename) as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        for point in points:
            drawer.ellipse([point[0]-4,point[1]-4,point[0]+4,point[1]+4],fill=None,outline=(255,255,51,255))
        del drawer
        img.save(filename)

def draw_votes(points,img_file,out_file):
    with Image.open(img_file).convert("RGBA") as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        color_code = {1: (0,0,0,255),2:(0,0,255,255),3:(255,0,0,255),4:(0,255,0,255),5:(255,20,147,64),6:(139,69,19,64)}

        for point in points:
            drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],outline=color_code[point[2]])

        del drawer
        img.save(out_file)

def create_json(filename):
    with open(filename) as file:
        file_reader = csv.reader(file,skipinitialspace=True)
        next(file_reader)
        subj_data = {}
        for line in file_reader:

            id = int(line[0])

            file_info = json.loads(line[4])
            subj_data[id] = file_info

    with open(filename.strip('csv')+'json','w') as outfile:
        json.dump(subj_data,outfile)

def draw_circles(points,img_file,out_file):
    with Image.open(img_file).convert("RGB") as img:
        
        drawer = ImageDraw.Draw(img)

        colors = ['red','blue','orange','purple','pink','yellow']

        for point_set in points:
            color = colors.pop(0)
            for point in point_set:
                drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],fill=None,outline=color)



        del drawer
        img.save(out_file)

    
if __name__ == "__main__":
    main()
